import { registerBlockType } from "@wordpress/blocks";
const { __ } = wp.i18n;
import { InspectorControls } from "@wordpress/block-editor";
const { Component } = wp.element;
import apiFetch from "@wordpress/api-fetch";
import { SelectControl } from "@wordpress/components";
import "./style.scss";

class mySelectPosts extends Component {
	static getInitialState(selectedPost) {
		return {
			posts: [],
			selectedPost: selectedPost,
			post: {},
		};
	}

	constructor() {
		super(...arguments);
		this.state = this.constructor.getInitialState(
			this.props.attributes.selectedPost
		);
		this.getOptions = this.getOptions.bind(this);
		this.getOptions();
		this.onChangeSelectPost = this.onChangeSelectPost.bind(this);
	}

	onChangeSelectPost(value) {
		const post = this.state.posts.find((item) => {
			return item.id == parseInt(value);
		});
		this.setState({ selectedPost: parseInt(value), post });
		this.props.setAttributes({
			selectedPost: parseInt(value),
			title: post.title.rendered,
			content: post.excerpt.rendered,
			link: post.link,
		});
	}

	getOptions() {
		return apiFetch({ path: "/wp/v2/posts" }).then((posts) => {
			if (posts && 0 !== this.state.selectedPost) {
				const post = posts.find((item) => {
					return item.id == this.state.selectedPost;
				});
				this.setState({ post, posts });
			} else {
				this.setState({ posts });
			}
		});
	}

	render() {
		let options = [{ value: 0, label: __("Select a Post") }];
		let output = __("Loading Posts");
		if (this.state.posts.length > 0) {
			const loading = __("We have %d posts. Choose one.");
			output = loading.replace("%d", this.state.posts.length);
			this.state.posts.forEach((post) => {
				options.push({ value: post.id, label: post.title.rendered });
			});
		} else {
			output = __("No posts found. Please create some first.");
		}
		if (this.state.post.hasOwnProperty("title")) {
			output = (
				<div className="post">
					<a href={this.state.post.link}>
						<h2
							dangerouslySetInnerHTML={{
								__html: this.state.post.title.rendered,
							}}
						></h2>
					</a>
					<p
						dangerouslySetInnerHTML={{
							__html: this.state.post.excerpt.rendered,
						}}
					></p>
				</div>
			);
		}
		return [
			!!this.props.isSelected && (
				<InspectorControls key="inspector">
					<SelectControl
						onChange={this.onChangeSelectPost}
						value={this.props.attributes.selectedPost}
						label={__("Select a Post")}
						options={options}
					/>
				</InspectorControls>
			),
			<div className={this.props.className}>{output}</div>,
		];
	}
}

registerBlockType("kaplan-blocks/embed-post-sample", {
	title: __("Embed post sample"), 
	icon: "shield", 
	category: "text", 
	keywords: [__("embed"), __("Embed Post"), __("kaplan")],
	attributes: {
		content: {
			type: "array",
			source: "children",
			selector: "p",
		},
		title: {
			type: "string",
			selector: "h2",
		},
		link: {
			type: "string",
			selector: "a",
		},
		selectedPost: {
			type: "number",
			default: 0,
		},
	},
	edit: mySelectPosts,
	save: function (props) {
		return (
			<div className={props.className}>
				<div className="post">
					<a href={props.attributes.link}>
						<h2
							dangerouslySetInnerHTML={{ __html: props.attributes.title }}
						></h2>
					</a>
					<p dangerouslySetInnerHTML={{ __html: props.attributes.content }}></p>
				</div>
			</div>
		);
	},
});
