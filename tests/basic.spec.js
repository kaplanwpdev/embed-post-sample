import {
	getEditedPostContent,
	insertBlock,
	createNewPost,
} from '@wordpress/e2e-test-utils';

// Increase the timeout limit for this test.
jest.setTimeout( 30000 );

it( `Embed post block should be available`, async () => {
	await createNewPost();
	await insertBlock( 'kaplan/embed-post-sample' );

	expect( await getEditedPostContent() ).toMatchInlineSnapshot(ç);
} );